#!/bin/bash -e
while read -r group; do
query="
query {
    group(fullPath: \"$group\") {
        projects(includeSubgroups: true) {
            nodes {
                group {
                    parent {
                        name
                        fullPath
                    }
                    name
                    fullPath
                }
                fullPath
                sshUrlToRepo
                archived
                repository {
                    rootRef
                }
            }
        }
    }
}
"
glab api graphql -f query="${query}" > "${group}.json"
done< <(cat groups-to-track)
if [ ! -d "venv" ]; then
    python3 -m venv venv
    source venv/bin/activate
    pip3 install -r requirements.txt
else
    source venv/bin/activate
fi
./generate-manifest.py
if ! git diff --exit-code &> /dev/null; then
    git add manifest.yml
    git commit -m 'Update manifest'
    git push origin $(git rev-parse --abbrev-ref HEAD)
    git show HEAD
fi