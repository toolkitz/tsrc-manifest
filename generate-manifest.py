#!/usr/bin/env python3
import json
import yaml

with open('groups-to-track', 'r') as _f:
    groups=[line.strip() for line in _f.readlines()]

repos_per_group = {}
for group in groups:
    with open(f'{group}.json', 'r') as _f:
        content = json.loads(_f.read())['data']['group']
        if content:
            repos_per_group[group] = content['projects']['nodes']


all_repos = []
all_groups = {}
for group, repos in repos_per_group.items():
    repos = [repo for repo in repos if not repo['archived']]
    for repo in repos:
        if not repo['repository']:
            continue
        # work around for wiki url
        url = 'git@gitlab.com:toolkitz/docs.wiki.git' if repo['sshUrlToRepo'] == 'git@gitlab.com:toolkitz/docs.git' else repo['sshUrlToRepo']
        all_repos.append({
            'url': url,
            'dest': repo['fullPath'],
            'branch': "master" if repo['repository']['rootRef'] == None else repo['repository']['rootRef'],
        })
        group_path = repo['group']['fullPath']
        if group_path not in all_groups:
            all_groups[group_path] = {
                'repos': set(),
                'includes': set(),
            }
        all_groups[group_path]['repos'].add(repo['fullPath'])
    for repo in repos:
        try:
            parent_path = repo['group']['parent']['fullPath']
            group_path = repo['group']['fullPath']
        except TypeError:
            continue
        if parent_path not in all_groups:
            all_groups[parent_path] = {
                'repos': set(),
                'includes': set(),
            }
        all_groups[parent_path]['includes'].add(group_path)

for group, group_config in all_groups.items():
    group_config['includes'] = sorted(list(group_config['includes']))
    group_config['repos'] = sorted(list(group_config['repos']))

with open('manifest.yml', 'w') as _f:
    _f.write(yaml.dump({
        'repos': all_repos,
        'groups': dict(all_groups)
    }))
