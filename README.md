[[_TOC_]]

# What is tsrc ?

TSRC is IaC (infrastructure as code) tool which helps in defining standard repository structure to check out and synchronize.

# When to use tsrc ?

If you need to manage multiple repositories and you start feeling confused where they are located tsrc is perfect tool to solve that problem.

# How to install tsrc ?

[check official docs](https://github.com/dmerejkowsky/tsrc#installation)

# How to init tsrc ?

## Not admin user

**NOTICE**: This is a little bit complicated than for admin user because you don't have access to all the repositories. In this tutorial you will be guided how to create your own tsrc branch and how to keep it in sync with the repositories you've got access to.

* Create your own branch (name of the branch: your gitlab user name) from the UI and delete `manifest.yml` file also from the UI.
* Now create an empty directory where you want to keep all of your repositories (for e.g.: `$HOME/repos/agmar`).
* Create file called `$HOME/repos/agmar/.tsrc/config.yml` with following content:

```yaml
manifest_url: git@gitlab.com:toolkitz/tsrc-manifest.git
manifest_branch: yourgitlabusername
repo_groups:
shallow_clones: true
clone_all_repos: true
singular_remote:
```

* Now clone run in this directory `git clone git@gitlab.com:toolkitz/tsrc-manifest.git manifest` to clone repo into `$HOME/repos/agmar/.tsrc/manifest`
* Enter in there and run update script `./generate-manifest.sh`. You need to have working [glab](https://github.com/profclems/glab) cli for that.
* Now finally run `tsrc sync` to get all the repos that you have access to.

## Admin user

let's say you've got directory `$HOME/repos/agmar` and in root of `agmar` directory you wish to keep all repos from this tsrc manifest. You need to enter `agmar` directory and run:

`tsrc init git@gitlab.com:toolkitz/tsrc-manifest.git --shallow`

worth to check [docs](https://dmerejkowsky.github.io/tsrc/ref/workspace-config/) on how to configure workspace to reduce ammount of pulls

# How to update tsrc ?

## Updating repos

Once you add / archive / delete some new repo you need to run `./generate-manifest.sh`.

## Updating root groups

If you need to start tracking new group add it to [`groups-to-track`](https://gitlab.com/toolkitz/tsrc-manifest/-/blob/master/groups-to-track) file. You don't need to add subgroups.

# How to pull changes ?

You can use:

`tsrc sync`

or

`tsrc sync -g terraform` if you wish to sync repos only from group named `terraform`

# How to push multiple changes ?

`tsrc foreach -g terraform -c 'git add -A; git commit -m "update deps"; git push'` -> this command will create and push commit in each repository from `terraform` group
